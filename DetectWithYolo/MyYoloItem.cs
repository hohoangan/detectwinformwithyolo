﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DetectWithYolo
{
    public class MyYoloItem
    {
        public string Type { get; set; }
        public double Confidence { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public Point Center;
        public bool Counted { get; set; }//true: đối tượng này đã được đếm
        public bool Tracked { get; set; } //true: đối tượng này đã được xác định frame trước và sau
        public MyYoloItem ObjectPrevious { get; set; }
    }
}
