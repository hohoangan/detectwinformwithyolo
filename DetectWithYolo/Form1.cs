﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;
using Alturos.Yolo;
using System.Threading;
using Emgu.CV.CvEnum;
using Alturos.Yolo.Model;
using System.Net;
using System.Text;
using System.ComponentModel;
using System.Configuration;
using RestSharp;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using WebSocketSharp;
using System.Net.NetworkInformation;
using System.Diagnostics;
using Microsoft.Win32;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace DetectWithYolo
{
    public partial class formMain : Form
    {
        Capture capture;
        YoloWrapper yoloWrapper;
        YoloWrapper yoloWrapperFULL;
        bool replay = true;
        bool isSave = true;
        double carcount = 0;
        double motocount = 0;
        double buscount = 0;
        int night = 18;
        int px1, px2, py1, py2;
        DateTime lastcount;
        Point px, py;
        int clickcount = 1;
        int WIDTH = 960, HEIGHT = 540;
        double autosizeMoto = 400;
        List<List<PointF>> lstLines = new List<List<PointF>>();
        List<MyYoloItem> lstObjectOld = new List<MyYoloItem>();

        List<string> DETECT = new List<string> { "bicycle", "motorbike", "bus", "truck", "car" };
        Dictionary<string, Color> lstColor = new Dictionary<string, Color>
        {
            { "bicycle", Color.AliceBlue},
            { "motorbike", Color.AliceBlue},
            { "bus", Color.Green},
            { "truck", Color.Violet},
            { "car", Color.Orange},
            { "person", Color.AliceBlue}
        };
        Dictionary<string, Brush> lstBrush = new Dictionary<string, Brush>
        {
            { "bicycle", Brushes.AliceBlue },
            { "motorbike", Brushes.AliceBlue},
            { "bus", Brushes.Green},
            { "truck", Brushes.Violet},
            { "car", Brushes.Orange},
            { "person", Brushes.AliceBlue}
        };

        /*VINSTAR CONFIGURE*/
        VinstarConfigure vinConfig = new VinstarConfigure();

        double confidence = 0.1;
        // model is available here:
        // https://github.com/onnx/models/tree/master/vision/object_detection_segmentation/yolov4

        BackgroundWorker backgroundWorker = new BackgroundWorker();
        //private WebSocket client;
        const string host = "ws://localhost:5000";
        //const string host = "wss://localhost:5001";
        public formMain()
        {
            InitializeComponent();
            try
            {
                //connectWebsocket();
                //client.Connect();
                checkDetectList();
                backgroundWorker.DoWork += BackgroundWorker_DoWork;
                backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
                getConfigure();
                setConfigFromFile2Form();
                chkPerson.Checked = vinConfig.CheckPerson;
                //StartStreaming();
                //check license
                var macAddr =
                (
                    from nic in NetworkInterface.GetAllNetworkInterfaces()
                    where nic.OperationalStatus == OperationalStatus.Up
                    select nic.GetPhysicalAddress().ToString()
                ).FirstOrDefault();

                var client = new RestClient(string.Format("{0}/api/billboard/counting/checklicense?macaddr={1}", vinConfig.Server, macAddr));
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                if (!response.Content.Equals("true"))
                {
                    MessageBox.Show("Check license:" + macAddr);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
                this.Close();
            }

        }

        #region connect websocket
        //private void connectWebsocket()
        //{
        //    client = new WebSocket(host);

        //    client.OnOpen += (ss, ee) =>
        //       MessageBox.Show(string.Format("connected to {0}successfully", host));
        //    client.OnError += (ss, ee) =>
        //       MessageBox.Show("Error: " +ee.Message);
        //    client.OnMessage += (ss, ee) =>
        //       MessageBox.Show("Echo: " +ee.Data);
        //    client.OnClose += (ss, ee) =>
        //       MessageBox.Show(string.Format("Disconnected with {0}", host));
        //}
        #endregion

        private void getConfigure()
        {
            try
            {
                vinConfig = new VinstarConfigure();
                string rstp = ConfigurationManager.AppSettings["rstp"];
                string email = ConfigurationManager.AppSettings["email"];
                string billboardId = ConfigurationManager.AppSettings["billboardId"];
                string billboardCameraId = ConfigurationManager.AppSettings["billboardCameraId"];
                string lines = ConfigurationManager.AppSettings["lines"];
                string minutes2reload = ConfigurationManager.AppSettings["minutes2reload"];
                string hours2savedata = ConfigurationManager.AppSettings["hours2savedata"];
                string confid = ConfigurationManager.AppSettings["confidences"];
                string distance = ConfigurationManager.AppSettings["distance"];
                string angle = ConfigurationManager.AppSettings["angle"];
                string frame = ConfigurationManager.AppSettings["frame"];
                string autostart = ConfigurationManager.AppSettings["autostart"];
                string showconfig = ConfigurationManager.AppSettings["showconfig"];
                string teamviewId = ConfigurationManager.AppSettings["teamviewId"];
                string teamviewPassword = ConfigurationManager.AppSettings["teamviewPassword"];
                string description = ConfigurationManager.AppSettings["description"];
                string server = ConfigurationManager.AppSettings["server"];
                string portStream = ConfigurationManager.AppSettings["portStream"];
                string chkPerson = ConfigurationManager.AppSettings["chkPerson"];
                string numericOnBus = ConfigurationManager.AppSettings["numericOnBus"];
                string chkOnBus = ConfigurationManager.AppSettings["chkOnBus"];
                string channel = ConfigurationManager.AppSettings["channel"];

                //init data
                vinConfig.RSTP = rstp;
                vinConfig.Email = email;
                vinConfig.BillboardId = int.Parse(billboardId);
                vinConfig.BillboardCameraId = int.Parse(billboardCameraId);
                vinConfig.Lines = lines;
                vinConfig.Minutes2reload = int.Parse(minutes2reload);
                vinConfig.Hours2savedata = int.Parse(hours2savedata);
                vinConfig.Confidences = int.Parse(confid);
                vinConfig.Distance = double.Parse(distance);
                vinConfig.Angle = double.Parse(angle);
                vinConfig.Frame = int.Parse(frame);
                vinConfig.Autostart = "1".Equals(autostart);
                vinConfig.ShowConfig = "1".Equals(showconfig);
                vinConfig.TeamviewId = teamviewId;
                vinConfig.TeamviewPassword = teamviewPassword;
                vinConfig.Description = description;
                vinConfig.Server = server;
                vinConfig.PortStream = int.Parse(portStream);
                vinConfig.CheckPerson = "1".Equals(chkPerson);
                vinConfig.CheckOnBus = "1".Equals(chkOnBus);
                vinConfig.NumericOnBus = int.Parse(numericOnBus);
                vinConfig.Channel = channel;
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
        }
        private void setConfigFromFile2Form()
        {
            try
            {
                if (vinConfig != new VinstarConfigure())
                {
                    txtRstp.Text = vinConfig.RSTP;
                    //load danh sách line đã config trước đó
                    if (!string.IsNullOrEmpty(vinConfig.Lines))
                    {
                        string[][] InFormOfStringArray = vinConfig.Lines.Split(';')
                                                        .Select(s => s.Trim().Split('|'))
                                                        .ToArray();
                        for (int i = 0; i < InFormOfStringArray.Length; i++)
                        {
                            if (InFormOfStringArray[i].Count() < 2)
                                continue;
                            var p1 = InFormOfStringArray[i][0].Split(',');
                            var p2 = InFormOfStringArray[i][1].Split(',');
                            PointF point1 = new PointF(float.Parse(p1[0]), float.Parse(p1[1]));
                            PointF point2 = new PointF(float.Parse(p2[0]), float.Parse(p2[1]));
                            lstLines.Add(new List<PointF> { point1, point2 });
                        }
                    }
                    trackConfidence.Value = vinConfig.Confidences;
                    confidence = (double)trackConfidence.Value / 10;
                    numericDistance.Value = (decimal)vinConfig.Distance;
                    numericAngle.Value = (decimal)vinConfig.Angle;
                    numericFrame.Value = vinConfig.Frame;
                    checkAutostart.Checked = vinConfig.Autostart;
                    chkOnBus.Checked = vinConfig.CheckOnBus;
                    chkPerson.Checked = vinConfig.CheckPerson;
                    numericOnBus.Value = vinConfig.NumericOnBus;
                    //show bảng config
                    Cogfigure.Visible = vinConfig.ShowConfig;
                    //tự động chạy chương trình đo đếm camera
                    if (vinConfig.Autostart == true)
                        rtsp_play();
                }
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
        }
        private void convertListLines2String()
        {
            string lines = string.Empty;
            try
            {
                for (int i = 0; i < lstLines.Count; i++)
                {
                    lines += string.Format("{0},{1}|{2},{3};", lstLines[i][0].X, lstLines[i][0].Y, lstLines[i][1].X, lstLines[i][1].Y);
                }
            }
            catch (Exception ex)
            {
                lines = string.Empty;
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
            finally
            {
                AppConfigChangeValue("lines", lines);
            }
        }

        private void AppConfigChangeValue(string name, object value)
        {
            try
            {
                Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                configuration.AppSettings.Settings[name].Value = value.ToString();
                configuration.Save();
                ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            backgroundWorker.RunWorkerAsync();
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime dt = DateTime.Now;
            if (lastcount != new DateTime())
            {
                double t = (dt - lastcount).TotalMinutes;
                //neu restart roi ma van chua len
                if ((vinConfig.Minutes2reload * 2) <= t)
                {
                    Gmail.SendMailTakePhoto(vinConfig.Email, vinConfig.BillboardId, vinConfig.BillboardCameraId);
                    replay = true;
                }
                if (t > vinConfig.Minutes2reload) //neu 2p roi ma chua dem dc xe
                {
                    reloadCamera();
                    Gmail.SendMailTakePhoto(vinConfig.Email, vinConfig.BillboardId, vinConfig.BillboardCameraId);
                }
                else
                    replay = true;
            }
        }

        private void OpenConfigureYolo()
        {
            //var configurationDetector = new YoloConfigurationDetector();
            //var config = configurationDetector.Detect();
            // yoloWrapper = new YoloWrapper(config);
            if (radioYoloTiny.Checked == true)
                yoloWrapper = new YoloWrapper("yolov4-tiny.cfg", "yolov4-tiny.weights", "yolov4.names");
            if(chkOnBus.Checked == true)
                yoloWrapperFULL = new YoloWrapper("yolov4.cfg", "yolov4.weights", "yolov4.names");
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            LoadVideo();
        }
        void LoadVideo()
        {
            if (capture == null)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Video mp4|*.mp4";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    textBox2.Text = ofd.FileName;
                    //capture = new Capture(ofd.FileName);
                }
            }
            //capture.ImageGrabbed += Capture_ImageGrabbed;
            //capture.Start();
        }
        private void mp4_play()
        {
            if (!String.IsNullOrEmpty(textBox2.Text))
            {
                CapStop();
                capture = new Capture(textBox2.Text);
                //cap.QueryFrame();
                capture.ImageGrabbed += Capture_ImageGrabbed;
                capture.Start();
            }
        }

        private void reloadCamera()
        {
            savedata();
            //if (DateTime.Now.Minute % vinConfig.Minutes2reload == 0 && replay == true)
            if (replay == true)
            {
                replay = false;
                rtsp_play();
            }
            //if (DateTime.Now.Minute % vinConfig.Minutes2reload != 0)
            //{
            //    replay = true;
            //}
        }
        private void sendData2SQL()
        {
            if (DateTime.Now.Minute == 0 && isSave == true)
            {
                isSave = false;
                savedata();
                showStartTime();
            }
            if (DateTime.Now.Minute != 0)
            {
                isSave = true;
            }
            //if (DateTime.Now.Hour % vinConfig.Hours2savedata == 0 && isSave == true)
            //{
            //    isSave = false;
            //    savedata();
            //    showStartTime();
            //}
            //if (DateTime.Now.Hour % vinConfig.Hours2savedata != 0)
            //{
            //    isSave = true;
            //}
        }
        private void Capture_ImageGrabbed(object sender, EventArgs e)
        {
            try
            {
                sendData2SQL();
                int fps = 1;
                if (capture == null)
                    return;
                else
                {
                    fps = (int)capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);

                    Mat m = new Mat();
                    capture.Retrieve(m);
                    //resize
                    if (checkResize.Checked == true)
                    {
                        int width = 960, height = 540;
                        CvInvoke.Resize(m, m, new Size(width, height), 0, 0, Inter.Linear);
                    }
                    WIDTH = m.Width;
                    HEIGHT = m.Height;

                    int currentFame = (int)Math.Floor(capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames));
                    //Thread.Sleep(fps);
                    /*
                        * Nếu cần show ra cách detect mới vẽ ra 
                        * còn không thì lấy video stream thoi
                        */
                    if (checkShowHideDetect.Checked != true)
                    {
                        Invoke((MethodInvoker)(delegate ()
                        {
                            pictureBox1.Image = (m.ToImage<Bgr, byte>()).Bitmap;
                        }));
                    }
                    if (chkOnBus.Checked == true || DateTime.Now.Hour >= night)
                    {
                        if (currentFame % (numericOnBus.Value * fps) == 0) // số giây chup 1 lan
                            DetecYolo(m.ToImage<Bgr, byte>());
                    }
                    else if (currentFame % numericFrame.Value == 0)
                    {
                        DetecYolo(m.ToImage<Bgr, byte>());
                    }
                }
            }
            catch (Exception)
            {
                savedata();
                CapStop();
                //string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                //MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
        }
        private string toB64img(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] imageBytes = ms.ToArray();
                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                return base64String;
            }
        }

        void DetecYolo(Image<Bgr, byte> imageColor)
        {
            using (var bitmap = imageColor.Bitmap)
            {
                using (var stream = new MemoryStream())
                {
                    imageColor.ToBitmap().Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    if (stream == null)
                        return;
                    var items = yoloWrapper.Detect(stream.ToArray());
                    if(chkOnBus.Checked == true || DateTime.Now.Hour >= night)
                    {
                        items = yoloWrapperFULL.Detect(stream.ToArray());
                    }
                    // bộ gom rác
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    using (var g = Graphics.FromImage(bitmap))
                    {
                        //vẽ line đang chọn trên màn hình
                        if (px != new Point() && py != new Point())
                        {
                            List<PointF> points = new List<PointF> { px, py };
                            points = convertPointFromClick2Img(points, bitmap.Width, bitmap.Height);
                            g.DrawLine(new Pen(Color.LightGreen, 3), points[0], points[1]);
                        }
                        //vẽ danh sách line dùng để đếm
                        if (lstLines.Count > 0 && checkShowHideLine.Checked == true)
                        {
                            lstLines.ForEach(item =>
                            {
                                g.DrawLine(new Pen(Color.Yellow, 3), item[0], item[1]);
                            });
                        }

                        items = items.Where(x => DETECT.Contains(x.Type) && x.Confidence >= confidence).ToList();

                        var lstMotor = items.Where(x => (x.Type == "motorbike" || x.Type == "person") && x.Confidence > 0.8).ToList();
                        double sizeMoto = lstMotor.Count > 0 ? lstMotor.Max(x => x.Height * x.Width) : 2000;
                        autosizeMoto = sizeMoto > autosizeMoto ? sizeMoto : autosizeMoto;
                        //neu co items thi ghi nhan lai thoi gian
                        lastcount = DateTime.Now;
                        //Danh sach object moi detect duoc
                        List<MyYoloItem> lstObjectNew = new List<MyYoloItem>();

                        foreach (var res in items)
                        {
                            Color color = lstColor[res.Type];
                            Brush brush = lstBrush[res.Type];
                            // "bicycle", "motorbike", "bus", "truck", "car" 
                            string sCar = string.IsNullOrEmpty(txtsize.Text.Trim()) ? "0" : txtsize.Text.Trim();
                            double sizeCar = double.Parse(sCar);
                            double size = res.Height * res.Width;
                            if (checkAutoSize.Checked == true)
                            {
                                /*
                                 * lay size car = size max cua motor * 20%
                                 */
                                sizeCar = autosizeMoto * 1.2;
                            }
                            Invoke((MethodInvoker)(delegate ()
                            {
                                txtsize.Text = sizeCar.ToString();
                            }));
                            //xe car có độ tin cậy < 60% và size bé hơn car thì chuyển thành xe moto
                            if (res.Type.Equals("car") && ((res.Confidence < 0.6 && sizeCar >= 0 && size <= sizeCar) || (res.Confidence < 0.3)))
                            {
                                res.Type = "motorbike";
                                color = Color.Crimson;
                                brush = Brushes.Crimson;
                            }
                            else if (res.Type.Equals("person"))
                            {
                                res.Type = "motorbike";
                                color = Color.Pink;
                                brush = Brushes.Pink;
                            }
                            else if (res.Type.Equals("truck") && res.Confidence < 0.7)
                            {
                                continue;
                            }
                            else if (res.Type.Equals("bus") && res.Confidence < 0.8)
                            {
                                continue;
                            }
                            #region ĐẾM XE
                            /*
                             * [1]. Nếu xe mới hoàn toàn, và không có trong danh sách cũ
                                - nếu cắt qua thì đếm
                             * [2]. Nếu xe nằm trong danh sách xuất hiện trước đó (tính khoảng cách)
                                - nếu vị trí mới cắt qua, mà vị trí cũ ko cắt thì đếm
                                - còn ko thì bỏ qua ko đếm
                             * [3]. đưa danh sách tracking mới trở thành danh sách cũ
                             */

                            //Kẻ 1 đường chéo hình chữ nhật của object xe được tìm thấy
                            //Ktra đường chéo đó có cắt lines đếm ko
                            PointF start1 = new PointF(res.X, res.Y);
                            PointF end1 = new PointF(res.X + res.Width, res.Y + res.Height);
                            PointF start2 = new PointF(res.X + res.Width, res.Y);
                            PointF end2 = new PointF(res.X, res.Y + res.Height);
                            if (chkOnBus.Checked == true || DateTime.Now.Hour >= night)
                            {
                                //đếm trên xe bus
                                //cứ có đối tượng là đếm
                                switch (res.Type)
                                {
                                    case "bicycle":
                                    case "motorbike":
                                        motocount++;
                                        break;
                                    case "bus":
                                        buscount++;
                                        break;
                                    case "car":
                                    case "truck":
                                        carcount++;
                                        break;
                                    default:
                                        motocount++;
                                        break;
                                }
                            }
                            else if(lstLines.Count > 0)
                            {
                                //Kiem tra có cắt lines ko

                                var checkCut = lstLines.Where(x => FindLineIntersection(start1, end1, x[0], x[1]) == true
                                                || FindLineIntersection(start2, end2, x[0], x[1])).FirstOrDefault();
                                //Tim xem xe có trong danh sách cũ hay chưa
                                /*
                                 * b1. tìm các góc từ new object đến các object cũ có góc >= 110
                                 * b2. sau đó lấy ra khoảng cách bé nhất đến điểm đó
                                 */
                                var checkExist = lstObjectOld.Where(x => x.Type.Equals(res.Type))
                                                        .Where(x => Distance2Point(res.Center(), x.Center) <= (double)numericDistance.Value)
                                                        //neu có đối trượng track thì ktra góc phải bé hơn 120*
                                                        .Where(x => AngleBetween2Lines(x.ObjectPrevious != null ?
                                                                                        x.ObjectPrevious.Center : new Point(), x.Center, res.Center()) >= (double)numericAngle.Value)
                                                        .Where(x => x.Tracked == false)
                                                        .OrderBy(x => Distance2Point(start1, new PointF(x.X, x.Y)))
                                                        //Tim then tiep theo co góc to nhất(ko bị gấp khúc)
                                                        //.OrderByDescending(x=> AngleBetween2Lines(x.ObjectPrevious != null ? x.ObjectPrevious.Center : new Point(), x.Center, res.Center()))
                                                        .FirstOrDefault();
                                //neu tim thay then object cu phu hợp thì xóa nó ra khỏi list cũ
                                if (checkExist != null)
                                    lstObjectOld.Remove(checkExist);

                                //Them vo listObjectNew
                                MyYoloItem newObject = new MyYoloItem
                                {
                                    Type = res.Type,
                                    Confidence = res.Confidence,
                                    X = res.X,
                                    Y = res.Y,
                                    Width = res.Width,
                                    Height = res.Height,
                                    Center = res.Center(),
                                    Counted = checkCut != null ? true : (checkExist?.Counted == true ? true : false),
                                    ObjectPrevious = checkExist
                                };
                                lstObjectNew.Add(newObject);

                                //vẽ đường đi
                                if (checkExist != null && checkTracking.Checked == true)
                                {
                                    var drawObject = newObject;
                                    var pointCurr = res.Center();
                                    while (drawObject.ObjectPrevious != null)
                                    {
                                        g.DrawLine(new Pen(color, 2), pointCurr, drawObject.Center);
                                        pointCurr = drawObject.Center;
                                        drawObject = drawObject.ObjectPrevious;
                                    }
                                    checkExist.Tracked = true;
                                }
                                //[1]
                                if ((checkCut != null && checkExist == null)
                                    ||
                                   //[2] neu no cắt qua, mà nó frame trước đã đếm thì mới đếm
                                   //(checkCut != null && checkCutOld == null)
                                   (checkCut != null && checkExist.Counted == false)
                                )
                                {
                                    switch (res.Type)
                                    {
                                        case "bicycle":
                                        case "motorbike":
                                            motocount++;
                                            break;
                                        case "bus":
                                            buscount++;
                                            break;
                                        case "car":
                                        case "truck":
                                            carcount++;
                                            break;
                                        default:
                                            motocount++;
                                            break;
                                    }
                                    //lastcount = DateTime.Now;
                                }
                            }
                            #endregion

                            //Hien thi so dem xe
                            PrintCounting();

                            #region draw predictions
                            if (checkShowHideDetect.Checked == true)
                            {
                                //g.DrawRectangle(Pens.Red, res.X, res.Y, res.Width, res.Height);
                                g.DrawRectangle(new Pen(color, 1), res.X, res.Y, res.Width, res.Height);
                                using (var brushes = new SolidBrush(Color.FromArgb(50, color)))
                                {
                                    g.FillRectangle(brushes, res.X, res.Y, res.Width, res.Height);
                                }
                                g.DrawString(res.Type + " " + res.Confidence.ToString("0.00"),
                                             new Font("Arial", 12), brush, new PointF(res.X, res.Y));
                            }
                            #endregion
                            //[3]
                            lstObjectOld = lstObjectNew;
                        }
                        //luu file
                        //bitmap.Save(Path.Combine(imageOutputFolder, "result.jpg" ));
                        if (checkShowHideDetect.Checked == true)
                        {
                            Invoke((MethodInvoker)(delegate ()
                            {
                                pictureBox1.Image = (new Image<Bgr, Byte>(bitmap)).Bitmap;
                            }));
                        }
                    }
                }
            }
            //}
        }

        void DetecYolo_ORG(Image<Bgr, byte> imageColor)
        {
            using (var bitmap = imageColor.Bitmap)
            {
                using (var stream = new MemoryStream())
                {
                    imageColor.ToBitmap().Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var items = yoloWrapper.Detect(stream.ToArray());
                    using (var g = Graphics.FromImage(bitmap))
                    {
                        //vẽ line đang chọn trên màn hình
                        if (px != new Point() && py != new Point())
                        {
                            List<PointF> points = new List<PointF> { px, py };
                            points = convertPointFromClick2Img(points, bitmap.Width, bitmap.Height);
                            g.DrawLine(new Pen(Color.LightGreen, 3), points[0], points[1]);
                        }
                        //vẽ danh sách line dùng để đếm
                        if (lstLines.Count > 0)
                        {
                            lstLines.ForEach(item =>
                            {
                                g.DrawLine(new Pen(Color.Yellow, 3), item[0], item[1]);
                            });
                        }

                        items = items.Where(x => DETECT.Contains(x.Type) && x.Confidence >= confidence).ToList();
                        //Danh sach object moi detect duoc
                        List<MyYoloItem> lstObjectNew = new List<MyYoloItem>();

                        foreach (var res in items)
                        {
                            #region ĐẾM XE
                            /*
                             * [1]. Nếu xe mới hoàn toàn, và không có trong danh sách cũ
                                - nếu cắt qua thì đếm
                             * [2]. Nếu xe nằm trong danh sách xuất hiện trước đó (tính khoảng cách)
                                - nếu vị trí mới cắt qua, mà vị trí cũ ko cắt thì đếm
                                - còn ko thì bỏ qua ko đếm
                             * [3]. đưa danh sách tracking mới trở thành danh sách cũ
                             */

                            //Kẻ 1 đường chéo hình chữ nhật của object xe được tìm thấy
                            //Ktra đường chéo đó có cắt lines đếm ko
                            PointF start1 = new PointF(res.X, res.Y);
                            PointF end1 = new PointF(res.X + res.Width, res.Y + res.Height);
                            PointF start2 = new PointF(res.X + res.Width, res.Y);
                            PointF end2 = new PointF(res.X, res.Y + res.Height);
                            if (lstLines.Count > 0)
                            {
                                //Kiem tra có cắt lines ko
                                var checkCut = lstLines.Where(x => FindLineIntersection(start1, end1, x[0], x[1]) == true
                                                || FindLineIntersection(start2, end2, x[0], x[1])).FirstOrDefault();
                                //Tim xem xe có trong danh sách cũ hay chưa
                                var checkExist = lstObjectOld.Where(x => x.Type.Equals(res.Type))
                                                        .Where(x => Distance2Point(res.Center(), x.Center) <= (double)numericDistance.Value)
                                                        .OrderBy(x => Distance2Point(start1, new PointF(x.X, x.Y)))
                                                        .FirstOrDefault();

                                //List<PointF> checkCutOld = null;
                                //if (checkExist != null)
                                //{
                                //    //ktra object cũ có cắt line hay ko
                                //    PointF pStartOld = new PointF(checkExist.X, checkExist.Y);
                                //    PointF pEndOld = new PointF(checkExist.X + checkExist.Width, checkExist.Y + checkExist.Height);
                                //    checkCutOld = lstLines.Where(x => FindLineIntersection(pStartOld, pEndOld, x[0], x[1])).FirstOrDefault();
                                //}
                                //[1]
                                if ((checkCut != null && checkExist == null)
                                    ||
                                   //[2] neu no cắt qua, mà nó frame trước đã đếm thì mới đếm
                                   //(checkCut != null && checkCutOld == null)
                                   (checkCut != null && checkExist.Counted == false)
                                )
                                {
                                    switch (res.Type)
                                    {
                                        case "bicycle":
                                        case "motorbike":
                                            motocount++;
                                            break;
                                        case "bus":
                                            buscount++;
                                            break;
                                        default:
                                            carcount++;
                                            break;
                                    }
                                }
                                //Them vo listObjectNew
                                lstObjectNew.Add(new MyYoloItem
                                {
                                    Type = res.Type,
                                    Confidence = res.Confidence,
                                    X = res.X,
                                    Y = res.Y,
                                    Width = res.Width,
                                    Height = res.Height,
                                    Center = res.Center(),
                                    Counted = checkCut != null ? true : (checkExist?.Counted == true ? true : false)
                                });
                            }
                            #endregion

                            //Hien thi so dem xe
                            PrintCounting();

                            #region draw predictions
                            Color color = lstColor[res.Type];
                            Brush brush = lstBrush[res.Type];
                            //g.DrawRectangle(Pens.Red, res.X, res.Y, res.Width, res.Height);
                            g.DrawRectangle(new Pen(color, 1), res.X, res.Y, res.Width, res.Height);
                            using (var brushes = new SolidBrush(Color.FromArgb(50, color)))
                            {
                                g.FillRectangle(brushes, res.X, res.Y, res.Width, res.Height);
                            }
                            g.DrawString(res.Type + " " + res.Confidence.ToString("0.00"),
                                         new Font("Arial", 12), brush, new PointF(res.X, res.Y));
                            #endregion
                        }
                        //[3]
                        lstObjectOld = lstObjectNew;

                        //luu file
                        //bitmap.Save(Path.Combine(imageOutputFolder, "result.jpg" ));
                        Invoke((MethodInvoker)(delegate ()
                        {
                            pictureBox1.Image = (new Image<Bgr, Byte>(bitmap)).Bitmap;
                        }));
                    }
                }
            }
            //}
        }

        void savedata()
        {
            try
            {
                BillboardCounting bb = new BillboardCounting();
                bb.BillboardId = vinConfig.BillboardId;
                bb.BillboardCameraId = vinConfig.BillboardCameraId;
                bb.DigitalBillboardId = vinConfig.BillboardId;
                bb.DigitalBillboardCameraId = vinConfig.BillboardCameraId;
                bb.MotoAmount = motocount;
                bb.CarAmount = carcount;
                bb.BusAmount = buscount;
                bb.LastUpdated = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                bb.Type = DateTime.Now.Hour >= 6 && DateTime.Now.Hour < 18 ? "Day" : "Night";
                bb.TeamViewId = vinConfig.TeamviewId;
                bb.TeamViewPasswords = vinConfig.TeamviewPassword;
                bb.Desciption = vinConfig.Description;

                var client = new RestClient(vinConfig.Server + "/api/Billboard/counting");
                if (vinConfig.Channel == "Led")
                    client = new RestClient(vinConfig.Server + "/api/DigitalBillboard/counting");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = new JavaScriptSerializer().Serialize(bb);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
            finally
            {
                Reset();
                Console.WriteLine("Executing finally block.");
            }
        }

        #region XU LY VIDEO
        private bool CheckLines(MyYoloItem newObject)
        {
            //vẽ đường đi
            var pointCurr = newObject.Center;
            while (newObject.ObjectPrevious != null)
            {
                if (newObject.ObjectPrevious.ObjectPrevious == null)
                    return true;

                if (AngleBetween2Lines(newObject.ObjectPrevious.ObjectPrevious.Center, newObject.ObjectPrevious.Center, newObject.Center) <= 90)
                    return false;

                newObject = newObject.ObjectPrevious;
            }
            return true;
        }
        private double AngleBetween2Lines(Point p1, Point p2, Point p3)
        {
            //TÍNH GÓC P12^3
            //neu chi co 2 diem, = 180*
            if (p1 == new Point())
                return 180;
            //Goc 90*
            //p1 = new Point(10, 10);
            //p2 = new Point(10, 20);
            //p3 = new Point(20, 25);
            double x1 = p1.X;
            double x2 = p2.X;
            double x3 = p3.X;
            double y1 = p1.Y;
            double y2 = p2.Y;
            double y3 = p3.Y;

            double theta1 = Math.Atan2(y3 - y2, x3 - x2);
            double theta2 = Math.Atan2(y1 - y2, x1 - x2);
            double angle = Math.Abs(theta1 - theta2) * 180 / Math.PI;
            return angle;
        }

        private double Distance2Point(PointF p1, PointF p2)
        {
            return Math.Sqrt((Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2)));
        }
        public static bool FindLineIntersection(PointF start1, PointF end1, PointF start2, PointF end2)
        {
            float denom = ((end1.X - start1.X) * (end2.Y - start2.Y)) - ((end1.Y - start1.Y) * (end2.X - start2.X));
            //  AB & CD are parallel 
            if (denom == 0)
            {
                return false;
                //return PointF.Empty;
            }
            float numer = ((start1.Y - start2.Y) * (end2.X - start2.X)) - ((start1.X - start2.X) * (end2.Y - start2.Y));
            float r = numer / denom;
            float numer2 = ((start1.Y - start2.Y) * (end1.X - start1.X)) - ((start1.X - start2.X) * (end1.Y - start1.Y));
            float s = numer2 / denom;
            /*
            if ((r < 0 || r > 1) || (s < 0 || s > 1))
                return PointF.Empty;
            // Find intersection point
            PointF result = new PointF();
            result.X = start1.X + (r * (end1.X - start1.X));
            result.Y = start1.Y + (r * (end1.Y - start1.Y));
            return result;
            */
            if ((r < 0 || r > 1) || (s < 0 || s > 1))
                return false;
            return true;
        }

        private List<PointF> convertPointFromClick2Img(List<PointF> points, int imgW, int imgH)
        {
            //size cua picture box
            int picW = pictureBox1.Width;
            int picH = pictureBox1.Height;
            double scale = (double)imgW / picW;
            points = points.Select(x => { x.X = (int)(x.X * scale); x.Y = (int)(x.Y * scale); return x; }).ToList();
            return points;
        }
        private void buttonRtspPlay_Click(object sender, EventArgs e)
        {
            showStartTime();
            rtsp_play();
        }

        private void showStartTime()
        {
            MethodInvoker showDateInvokerDelegate = delegate ()
            {
                lbDatetime.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            };
            //This will be true if Current thread is not UI thread.
            if (this.InvokeRequired)
                this.Invoke(showDateInvokerDelegate);
            else
                showDateInvokerDelegate();
        }

        private void buttonMP4Play_Click(object sender, EventArgs e)
        {
            CapStop();
            OpenConfigureYolo();
            mp4_play();
        }

        private void buttonSetLine_Click(object sender, EventArgs e)
        {
            AddLine();
        }

        void ResetPoint()
        {
            textBoxx1.Text = string.Empty;
            textBoxx2.Text = string.Empty;
            textBoxy1.Text = string.Empty;
            textBoxy2.Text = string.Empty;
            px = new Point();
            py = new Point();
        }
        void AddLine()
        {
            try
            {
                px1 = Convert.ToInt32(textBoxx1.Text);
                px2 = Convert.ToInt32(textBoxx2.Text);
                py1 = Convert.ToInt32(textBoxy1.Text);
                py2 = Convert.ToInt32(textBoxy2.Text);
                lstLines.Add(CreatePolygonFrom2Point(new Point(px1, px2), new Point(py1, py2)));
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
            finally
            {
                ResetPoint();
                convertListLines2String();
            }
        }
        private List<PointF> CreatePolygonFrom2Point(PointF px, PointF py)
        {
            //ve hinh chu nhat noi đếm xe
            if (px != new Point() && py != new Point())
            {
                List<PointF> points = new List<PointF> { px, py };
                return points = convertPointFromClick2Img(points, WIDTH, HEIGHT);
            }
            return null;
        }
        private void buttonStop_Click(object sender, EventArgs e)
        {
            savedata();
            CapStop();
        }
        private void buttonReset_Click(object sender, EventArgs e)
        {
            Reset();
        }
        private void trackConfidence_Scroll(object sender, EventArgs e)
        {
            try
            {
                confidence = (double)trackConfidence.Value / 10;
                AppConfigChangeValue("confidences", trackConfidence.Value);
            }
            catch { }
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            lstLines = new List<List<PointF>>();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            savedata();
        }

        private void numericFrame_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                AppConfigChangeValue("frame", numericFrame.Value);
            }
            catch { }
        }

        private void numericDistance_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                AppConfigChangeValue("distance", numericDistance.Value);
            }
            catch { }
        }

        private void numericAngle_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                AppConfigChangeValue("angle", numericAngle.Value);
            }
            catch { }
        }

        private void checkAutostart_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                AppConfigChangeValue("autostart", checkAutostart.Checked == true ? "1" : "0");
            }
            catch { }
        }

        //click lay diem tren pic
        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (clickcount == 1)
            {
                textBoxx1.Text = e.X.ToString();
                textBoxx2.Text = e.Y.ToString();
                clickcount++;
                px = new Point(e.X, e.Y);

            }
            else if (clickcount == 2)
            {
                textBoxy1.Text = e.X.ToString();
                textBoxy2.Text = e.Y.ToString();
                clickcount--;
                py = new Point(e.X, e.Y);
            }
        }
        private async void rtsp_play()
        {
            try
            {
                //giai phong bo nho
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                lastcount = DateTime.Now;
                OpenConfigureYolo();
                if (!String.IsNullOrEmpty(txtRstp.Text))
                {
                    CapStop();
                    await Task.Run(() =>
                    {
                        var t = new Thread(() =>
                        {
                            Thread.BeginThreadAffinity();
                            capture = new Capture(txtRstp.Text);
                        });
                        t.SetApartmentState(ApartmentState.MTA);
                        t.Priority = ThreadPriority.Highest;
                        t.IsBackground = false;
                        t.Start();
                        while (true)
                        {
                            Thread.Sleep(TimeSpan.FromMilliseconds(10));
                            if (capture != default)
                            {
                                break;
                            }
                        }
                    });

                    //if (capture == null)
                    //    capture = new Capture(txtRstp.Text);
                    //capture.QueryFrame();
                    if (capture != null)
                    {
                        capture.ImageGrabbed += Capture_ImageGrabbed;
                        capture.Start();
                    }
                }
                if (!backgroundWorker.IsBusy)
                {
                    backgroundWorker.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
        }
        private void CapStop()
        {
            try
            {
                if (capture != null)
                {
                    capture.ImageGrabbed -= Capture_ImageGrabbed;
                    //giai phong 
                    capture.Dispose();
                    capture = null;
                }
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
        }
        void Reset()
        {
            carcount = 0;
            motocount = 0;
            buscount = 0;
            PrintCounting();
        }
        void PrintCounting()
        {
            MethodInvoker methodInvokerDelegate = delegate ()
            {
                lbTotalMoto.Text = motocount.ToString();
                lbTotalCar.Text = carcount.ToString();
                lbTotalBus.Text = buscount.ToString();
            };
            //This will be true if Current thread is not UI thread.
            if (this.InvokeRequired)
                this.Invoke(methodInvokerDelegate);
            else
                methodInvokerDelegate();
        }

        private void formMain_Load(object sender, EventArgs e)
        {
            showStartTime();
        }

        private void formMain_Move(object sender, EventArgs e)
        {
            Point p = this.PointToScreen(tabControl1.Location);
            Constant.Position = p;
        }

        #endregion

        #region STREAMING
        private ImageStreamingServer _Server;

        private void btnCalSize_Click(object sender, EventArgs e)
        {
            try
            {
                double h = Math.Abs(px.X - py.X);
                double w = Math.Abs(px.Y - py.Y);
                double s = h * w;
                txtsize.Text = s.ToString();
            }
            catch (Exception)
            {
                txtsize.Text = "0";
            }
        }

        private void chkPerson_CheckedChanged(object sender, EventArgs e)
        {
            checkDetectList();
            try
            {
                AppConfigChangeValue("chkPerson", chkPerson.Checked == true ? "1" : "0");
            }
            catch { }
        }

        private void checkDetectList()
        {
            if (chkPerson.Checked == true)
            {
                DETECT = new List<string> { "bicycle", "bus", "truck", "car", "person" };
            }
            else
            {
                DETECT = new List<string> { "bicycle", "motorbike", "bus", "truck", "car" };
            }
        }

        //this.linkLabel1.Text = string.Format("http://{0}:8080", "192.168.1.112");
        void StartStreaming()
        {
            try
            {
                Point p = this.PointToScreen(tabControl1.Location);
                Constant.Position = p;
                _Server = new ImageStreamingServer(tabControl1.Width, tabControl1.Height);
                _Server.Start(vinConfig.PortStream);
            }
            catch (Exception)
            {
            }
        }
        private DateTime time = DateTime.MinValue;

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void chkOnBus_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                AppConfigChangeValue("chkOnBus", chkOnBus.Checked == true ? "1" : "0");
            }
            catch { }
        }

        private void radioYolo_CheckedChanged(object sender, EventArgs e)
        {
            OpenConfigureYolo();
        }

        private void radioYoloTiny_CheckedChanged(object sender, EventArgs e)
        {
            OpenConfigureYolo();
        }

        private void numericOnBus_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                AppConfigChangeValue("numericOnBus", numericOnBus.Value);
            }
            catch { }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int count = (_Server.Clients != null) ? _Server.Clients.Count() : 0;
            //this.sts.Text = "Clients: " + count.ToString();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //System.Diagnostics.Process.Start("chrome", this.linkLabel1.Text);
        }
        #endregion

        #region QUAY VIDEO DIGITAL
        private void LoadExcel()
        {
            string filename = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JSON file |*.json";
            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filename = openFileDialog.FileName;
                    string content = System.IO.File.ReadAllText(filename);
                    JObject json = JObject.Parse(content);
                    JavaScriptSerializer j = new JavaScriptSerializer();
                    object a = j.Deserialize(content, typeof(object));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
            finally { }
        }
        #endregion
        private void makeAvi()
        {
            LoadExcel();
            return;
            // reads all images in folder 
            using (var process = new Process())
            {
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.FileName = @"ffmpeg.exe";
                process.StartInfo.Arguments = @"-y -framerate 1/2 -start_number 01 -i img\IMG_%2d.png -vf scale=720:756 -c:v libx264 -r 30 -pix_fmt yuv420p img\out.mp4";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.BeginErrorReadLine();
                process.WaitForExit();
            }
        }

        private void btnStartTakePicture_Click(object sender, EventArgs e)
        {
            makeAvi();
        }
    }

}
