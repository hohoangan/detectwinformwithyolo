﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DetectWithYolo
{
    public class VinstarConfigure
    {
        public string RSTP { get; set; }
        public string Email { get; set; }
        public int BillboardId { get; set; }
        public int BillboardCameraId { get; set; }
        public int PortStream { get; set; }
        public string Lines { get; set; }
        //thoi gian reload camera: unit = minutes
        public int Minutes2reload { get; set; }
        //số giờ mỗi lần save data: unit = hour
        public int Hours2savedata { get; set; }
        public int Confidences { get; set; }
        public double Distance { get; set; }
        public double Angle { get; set; }
        public int Frame { get; set; }
        public bool Autostart { get; set; }
        public bool ShowConfig { get; set; }
        public bool CheckPerson { get; set; }
        public bool CheckOnBus { get; set; }
        public int NumericOnBus { get; set; }
        public string TeamviewId { get; set; }
        public string TeamviewPassword { get; set; }
        public string Description { get; set; }
        public string Server { get; set; }
        public string Channel { get; set; }
    }
}
