﻿namespace DetectWithYolo
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBrowse = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRtspPlay = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonMP4Play = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.txtRstp = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkPerson = new System.Windows.Forms.CheckBox();
            this.checkAutoSize = new System.Windows.Forms.CheckBox();
            this.btnCalSize = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.txtsize = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.buttonSetLine = new System.Windows.Forms.Button();
            this.checkShowHideLine = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxx1 = new System.Windows.Forms.TextBox();
            this.textBoxy1 = new System.Windows.Forms.TextBox();
            this.textBoxy2 = new System.Windows.Forms.TextBox();
            this.textBoxx2 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbDatetime = new System.Windows.Forms.Label();
            this.lbTotalBus = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbTotalMoto = new System.Windows.Forms.Label();
            this.lbTotalCar = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnStartTakePicture = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.numericFrame = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numericDistance = new System.Windows.Forms.NumericUpDown();
            this.radioYoloTiny = new System.Windows.Forms.RadioButton();
            this.radioYolo = new System.Windows.Forms.RadioButton();
            this.Cogfigure = new System.Windows.Forms.GroupBox();
            this.chkOnBus = new System.Windows.Forms.CheckBox();
            this.checkAutostart = new System.Windows.Forms.CheckBox();
            this.checkShowHideDetect = new System.Windows.Forms.CheckBox();
            this.checkTracking = new System.Windows.Forms.CheckBox();
            this.numericAngle = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.trackConfidence = new System.Windows.Forms.TrackBar();
            this.checkResize = new System.Windows.Forms.CheckBox();
            this.numericOnBus = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDistance)).BeginInit();
            this.Cogfigure.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackConfidence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOnBus)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(1280, 236);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(54, 23);
            this.btnBrowse.TabIndex = 0;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleName = "";
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(980, 719);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(972, 693);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Stream Video";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.buttonRtspPlay);
            this.groupBox2.Controls.Add(this.buttonReset);
            this.groupBox2.Controls.Add(this.buttonMP4Play);
            this.groupBox2.Controls.Add(this.buttonStop);
            this.groupBox2.Controls.Add(this.txtRstp);
            this.groupBox2.Location = new System.Drawing.Point(507, 616);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(456, 74);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Open File";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Rtsp Url:";
            // 
            // buttonRtspPlay
            // 
            this.buttonRtspPlay.Location = new System.Drawing.Point(59, 43);
            this.buttonRtspPlay.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRtspPlay.Name = "buttonRtspPlay";
            this.buttonRtspPlay.Size = new System.Drawing.Size(76, 22);
            this.buttonRtspPlay.TabIndex = 30;
            this.buttonRtspPlay.Text = "Rtsp";
            this.buttonRtspPlay.UseVisualStyleBackColor = true;
            this.buttonRtspPlay.Click += new System.EventHandler(this.buttonRtspPlay_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(314, 43);
            this.buttonReset.Margin = new System.Windows.Forms.Padding(2);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(76, 22);
            this.buttonReset.TabIndex = 38;
            this.buttonReset.Text = " Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            // 
            // buttonMP4Play
            // 
            this.buttonMP4Play.Location = new System.Drawing.Point(142, 43);
            this.buttonMP4Play.Margin = new System.Windows.Forms.Padding(2);
            this.buttonMP4Play.Name = "buttonMP4Play";
            this.buttonMP4Play.Size = new System.Drawing.Size(76, 22);
            this.buttonMP4Play.TabIndex = 31;
            this.buttonMP4Play.Text = "MP4 Play";
            this.buttonMP4Play.UseVisualStyleBackColor = true;
            this.buttonMP4Play.Click += new System.EventHandler(this.buttonMP4Play_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(226, 43);
            this.buttonStop.Margin = new System.Windows.Forms.Padding(2);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(76, 22);
            this.buttonStop.TabIndex = 37;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // txtRstp
            // 
            this.txtRstp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.txtRstp.Location = new System.Drawing.Point(58, 12);
            this.txtRstp.Margin = new System.Windows.Forms.Padding(2);
            this.txtRstp.Name = "txtRstp";
            this.txtRstp.Size = new System.Drawing.Size(393, 24);
            this.txtRstp.TabIndex = 33;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkPerson);
            this.groupBox3.Controls.Add(this.checkAutoSize);
            this.groupBox3.Controls.Add(this.btnCalSize);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtsize);
            this.groupBox3.Controls.Add(this.btnClear);
            this.groupBox3.Controls.Add(this.buttonSetLine);
            this.groupBox3.Controls.Add(this.checkShowHideLine);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.textBoxx1);
            this.groupBox3.Controls.Add(this.textBoxy1);
            this.groupBox3.Controls.Add(this.textBoxy2);
            this.groupBox3.Controls.Add(this.textBoxx2);
            this.groupBox3.Location = new System.Drawing.Point(8, 616);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(493, 74);
            this.groupBox3.TabIndex = 50;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Add Polygon";
            // 
            // chkPerson
            // 
            this.chkPerson.AutoSize = true;
            this.chkPerson.Checked = true;
            this.chkPerson.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPerson.Location = new System.Drawing.Point(421, 18);
            this.chkPerson.Name = "chkPerson";
            this.chkPerson.Size = new System.Drawing.Size(59, 17);
            this.chkPerson.TabIndex = 57;
            this.chkPerson.Text = "Person";
            this.chkPerson.UseVisualStyleBackColor = true;
            this.chkPerson.CheckedChanged += new System.EventHandler(this.chkPerson_CheckedChanged);
            // 
            // checkAutoSize
            // 
            this.checkAutoSize.AutoSize = true;
            this.checkAutoSize.Location = new System.Drawing.Point(274, 19);
            this.checkAutoSize.Name = "checkAutoSize";
            this.checkAutoSize.Size = new System.Drawing.Size(68, 17);
            this.checkAutoSize.TabIndex = 56;
            this.checkAutoSize.Text = "AutoSize";
            this.checkAutoSize.UseVisualStyleBackColor = true;
            // 
            // btnCalSize
            // 
            this.btnCalSize.Location = new System.Drawing.Point(274, 41);
            this.btnCalSize.Margin = new System.Windows.Forms.Padding(2);
            this.btnCalSize.Name = "btnCalSize";
            this.btnCalSize.Size = new System.Drawing.Size(65, 24);
            this.btnCalSize.TabIndex = 55;
            this.btnCalSize.Text = "Cal size";
            this.btnCalSize.UseVisualStyleBackColor = true;
            this.btnCalSize.Click += new System.EventHandler(this.btnCalSize_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(215, 22);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 13);
            this.label16.TabIndex = 54;
            this.label16.Text = "Size car";
            // 
            // txtsize
            // 
            this.txtsize.Location = new System.Drawing.Point(214, 45);
            this.txtsize.Margin = new System.Windows.Forms.Padding(2);
            this.txtsize.Name = "txtsize";
            this.txtsize.Size = new System.Drawing.Size(56, 20);
            this.txtsize.TabIndex = 53;
            this.txtsize.Text = "2000";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(421, 41);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(65, 24);
            this.btnClear.TabIndex = 30;
            this.btnClear.Text = "Clear Line";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // buttonSetLine
            // 
            this.buttonSetLine.Location = new System.Drawing.Point(348, 41);
            this.buttonSetLine.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSetLine.Name = "buttonSetLine";
            this.buttonSetLine.Size = new System.Drawing.Size(69, 24);
            this.buttonSetLine.TabIndex = 29;
            this.buttonSetLine.Text = "Add LINE";
            this.buttonSetLine.UseVisualStyleBackColor = true;
            this.buttonSetLine.Click += new System.EventHandler(this.buttonSetLine_Click);
            // 
            // checkShowHideLine
            // 
            this.checkShowHideLine.AutoSize = true;
            this.checkShowHideLine.Location = new System.Drawing.Point(352, 18);
            this.checkShowHideLine.Name = "checkShowHideLine";
            this.checkShowHideLine.Size = new System.Drawing.Size(53, 17);
            this.checkShowHideLine.TabIndex = 52;
            this.checkShowHideLine.Text = "SLine";
            this.checkShowHideLine.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Point 1 (X, Y):";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(114, 22);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Point 2 (X, Y):";
            // 
            // textBoxx1
            // 
            this.textBoxx1.Location = new System.Drawing.Point(9, 45);
            this.textBoxx1.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxx1.Name = "textBoxx1";
            this.textBoxx1.Size = new System.Drawing.Size(46, 20);
            this.textBoxx1.TabIndex = 23;
            // 
            // textBoxy1
            // 
            this.textBoxy1.Location = new System.Drawing.Point(113, 45);
            this.textBoxy1.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxy1.Name = "textBoxy1";
            this.textBoxy1.Size = new System.Drawing.Size(46, 20);
            this.textBoxy1.TabIndex = 24;
            // 
            // textBoxy2
            // 
            this.textBoxy2.Location = new System.Drawing.Point(162, 45);
            this.textBoxy2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxy2.Name = "textBoxy2";
            this.textBoxy2.Size = new System.Drawing.Size(46, 20);
            this.textBoxy2.TabIndex = 28;
            // 
            // textBoxx2
            // 
            this.textBoxx2.Location = new System.Drawing.Point(59, 45);
            this.textBoxx2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxx2.Name = "textBoxx2";
            this.textBoxx2.Size = new System.Drawing.Size(46, 20);
            this.textBoxx2.TabIndex = 26;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(966, 540);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbDatetime);
            this.groupBox1.Controls.Add(this.lbTotalBus);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lbTotalMoto);
            this.groupBox1.Controls.Add(this.lbTotalCar);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Location = new System.Drawing.Point(8, 549);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(955, 61);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Counting";
            // 
            // lbDatetime
            // 
            this.lbDatetime.AutoSize = true;
            this.lbDatetime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDatetime.ForeColor = System.Drawing.Color.Black;
            this.lbDatetime.Location = new System.Drawing.Point(638, 16);
            this.lbDatetime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbDatetime.Name = "lbDatetime";
            this.lbDatetime.Size = new System.Drawing.Size(72, 31);
            this.lbDatetime.TabIndex = 54;
            this.lbDatetime.Text = "Date";
            // 
            // lbTotalBus
            // 
            this.lbTotalBus.AutoSize = true;
            this.lbTotalBus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalBus.ForeColor = System.Drawing.Color.ForestGreen;
            this.lbTotalBus.Location = new System.Drawing.Point(419, 30);
            this.lbTotalBus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTotalBus.Name = "lbTotalBus";
            this.lbTotalBus.Size = new System.Drawing.Size(29, 31);
            this.lbTotalBus.TabIndex = 52;
            this.lbTotalBus.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(423, 19);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 13);
            this.label15.TabIndex = 50;
            this.label15.Text = "TOTAL BUS:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(559, 22);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 13);
            this.label13.TabIndex = 53;
            this.label13.Text = "START TIME:";
            // 
            // lbTotalMoto
            // 
            this.lbTotalMoto.AutoSize = true;
            this.lbTotalMoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalMoto.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.lbTotalMoto.Location = new System.Drawing.Point(245, 30);
            this.lbTotalMoto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTotalMoto.Name = "lbTotalMoto";
            this.lbTotalMoto.Size = new System.Drawing.Size(29, 31);
            this.lbTotalMoto.TabIndex = 49;
            this.lbTotalMoto.Text = "0";
            // 
            // lbTotalCar
            // 
            this.lbTotalCar.AutoSize = true;
            this.lbTotalCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalCar.ForeColor = System.Drawing.Color.Orange;
            this.lbTotalCar.Location = new System.Drawing.Point(50, 30);
            this.lbTotalCar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTotalCar.Name = "lbTotalCar";
            this.lbTotalCar.Size = new System.Drawing.Size(29, 31);
            this.lbTotalCar.TabIndex = 48;
            this.lbTotalCar.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(53, 19);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "TOTAL CAR - TRUCK:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(248, 19);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = "TOTAL MOTO:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnStartTakePicture);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(972, 693);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Take Picture";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnStartTakePicture
            // 
            this.btnStartTakePicture.Location = new System.Drawing.Point(6, 549);
            this.btnStartTakePicture.Name = "btnStartTakePicture";
            this.btnStartTakePicture.Size = new System.Drawing.Size(75, 23);
            this.btnStartTakePicture.TabIndex = 2;
            this.btnStartTakePicture.Text = "Start";
            this.btnStartTakePicture.UseVisualStyleBackColor = true;
            this.btnStartTakePicture.Click += new System.EventHandler(this.btnStartTakePicture_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(966, 540);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(989, 221);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "MP4 File Directory";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.textBox2.Location = new System.Drawing.Point(991, 236);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(284, 24);
            this.textBox2.TabIndex = 35;
            // 
            // numericFrame
            // 
            this.numericFrame.Location = new System.Drawing.Point(13, 66);
            this.numericFrame.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericFrame.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFrame.Name = "numericFrame";
            this.numericFrame.Size = new System.Drawing.Size(93, 20);
            this.numericFrame.TabIndex = 39;
            this.numericFrame.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericFrame.ValueChanged += new System.EventHandler(this.numericFrame_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "Frame count(fps/x)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Distance";
            // 
            // numericDistance
            // 
            this.numericDistance.Location = new System.Drawing.Point(13, 105);
            this.numericDistance.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericDistance.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericDistance.Name = "numericDistance";
            this.numericDistance.Size = new System.Drawing.Size(93, 20);
            this.numericDistance.TabIndex = 41;
            this.numericDistance.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.numericDistance.ValueChanged += new System.EventHandler(this.numericDistance_ValueChanged);
            // 
            // radioYoloTiny
            // 
            this.radioYoloTiny.AutoSize = true;
            this.radioYoloTiny.Checked = true;
            this.radioYoloTiny.Location = new System.Drawing.Point(13, 19);
            this.radioYoloTiny.Name = "radioYoloTiny";
            this.radioYoloTiny.Size = new System.Drawing.Size(45, 17);
            this.radioYoloTiny.TabIndex = 43;
            this.radioYoloTiny.TabStop = true;
            this.radioYoloTiny.Text = "Tiny";
            this.radioYoloTiny.UseVisualStyleBackColor = true;
            this.radioYoloTiny.CheckedChanged += new System.EventHandler(this.radioYoloTiny_CheckedChanged);
            // 
            // radioYolo
            // 
            this.radioYolo.AutoSize = true;
            this.radioYolo.Enabled = false;
            this.radioYolo.Location = new System.Drawing.Point(73, 19);
            this.radioYolo.Name = "radioYolo";
            this.radioYolo.Size = new System.Drawing.Size(41, 17);
            this.radioYolo.TabIndex = 44;
            this.radioYolo.Text = "Full";
            this.radioYolo.UseVisualStyleBackColor = true;
            this.radioYolo.CheckedChanged += new System.EventHandler(this.radioYolo_CheckedChanged);
            // 
            // Cogfigure
            // 
            this.Cogfigure.Controls.Add(this.label4);
            this.Cogfigure.Controls.Add(this.numericOnBus);
            this.Cogfigure.Controls.Add(this.chkOnBus);
            this.Cogfigure.Controls.Add(this.checkAutostart);
            this.Cogfigure.Controls.Add(this.checkShowHideDetect);
            this.Cogfigure.Controls.Add(this.checkTracking);
            this.Cogfigure.Controls.Add(this.numericAngle);
            this.Cogfigure.Controls.Add(this.label14);
            this.Cogfigure.Controls.Add(this.label10);
            this.Cogfigure.Controls.Add(this.label9);
            this.Cogfigure.Controls.Add(this.trackConfidence);
            this.Cogfigure.Controls.Add(this.checkResize);
            this.Cogfigure.Controls.Add(this.radioYoloTiny);
            this.Cogfigure.Controls.Add(this.label8);
            this.Cogfigure.Controls.Add(this.numericDistance);
            this.Cogfigure.Controls.Add(this.radioYolo);
            this.Cogfigure.Controls.Add(this.label7);
            this.Cogfigure.Controls.Add(this.numericFrame);
            this.Cogfigure.Dock = System.Windows.Forms.DockStyle.Top;
            this.Cogfigure.Location = new System.Drawing.Point(980, 0);
            this.Cogfigure.Name = "Cogfigure";
            this.Cogfigure.Size = new System.Drawing.Size(379, 218);
            this.Cogfigure.TabIndex = 45;
            this.Cogfigure.TabStop = false;
            this.Cogfigure.Text = "Configure";
            // 
            // chkOnBus
            // 
            this.chkOnBus.AutoSize = true;
            this.chkOnBus.Location = new System.Drawing.Point(200, 147);
            this.chkOnBus.Name = "chkOnBus";
            this.chkOnBus.Size = new System.Drawing.Size(61, 17);
            this.chkOnBus.TabIndex = 55;
            this.chkOnBus.Text = "On Bus";
            this.chkOnBus.UseVisualStyleBackColor = true;
            this.chkOnBus.CheckedChanged += new System.EventHandler(this.chkOnBus_CheckedChanged);
            // 
            // checkAutostart
            // 
            this.checkAutostart.AutoSize = true;
            this.checkAutostart.Location = new System.Drawing.Point(15, 183);
            this.checkAutostart.Name = "checkAutostart";
            this.checkAutostart.Size = new System.Drawing.Size(73, 17);
            this.checkAutostart.TabIndex = 54;
            this.checkAutostart.Text = "Auto Start";
            this.checkAutostart.UseVisualStyleBackColor = true;
            this.checkAutostart.CheckedChanged += new System.EventHandler(this.checkAutostart_CheckedChanged);
            // 
            // checkShowHideDetect
            // 
            this.checkShowHideDetect.AutoSize = true;
            this.checkShowHideDetect.Checked = true;
            this.checkShowHideDetect.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkShowHideDetect.Location = new System.Drawing.Point(126, 183);
            this.checkShowHideDetect.Name = "checkShowHideDetect";
            this.checkShowHideDetect.Size = new System.Drawing.Size(116, 17);
            this.checkShowHideDetect.TabIndex = 53;
            this.checkShowHideDetect.Text = "Show/Hide detect ";
            this.checkShowHideDetect.UseVisualStyleBackColor = true;
            // 
            // checkTracking
            // 
            this.checkTracking.AutoSize = true;
            this.checkTracking.Location = new System.Drawing.Point(126, 147);
            this.checkTracking.Name = "checkTracking";
            this.checkTracking.Size = new System.Drawing.Size(68, 17);
            this.checkTracking.TabIndex = 51;
            this.checkTracking.Text = "Tracking";
            this.checkTracking.UseVisualStyleBackColor = true;
            // 
            // numericAngle
            // 
            this.numericAngle.Location = new System.Drawing.Point(15, 144);
            this.numericAngle.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericAngle.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericAngle.Name = "numericAngle";
            this.numericAngle.Size = new System.Drawing.Size(93, 20);
            this.numericAngle.TabIndex = 50;
            this.numericAngle.Value = new decimal(new int[] {
            110,
            0,
            0,
            0});
            this.numericAngle.ValueChanged += new System.EventHandler(this.numericAngle_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 128);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(254, 13);
            this.label14.TabIndex = 49;
            this.label14.Text = "Angle > ( Góc giữa các đoạn thẳng tracking 90-180)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(129, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(229, 13);
            this.label10.TabIndex = 48;
            this.label10.Text = "0    10    20    30    40    50    60    70    80    90";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(123, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Confidences (%)";
            // 
            // trackConfidence
            // 
            this.trackConfidence.LargeChange = 1;
            this.trackConfidence.Location = new System.Drawing.Point(118, 66);
            this.trackConfidence.Name = "trackConfidence";
            this.trackConfidence.Size = new System.Drawing.Size(253, 45);
            this.trackConfidence.TabIndex = 46;
            this.trackConfidence.Value = 1;
            this.trackConfidence.Scroll += new System.EventHandler(this.trackConfidence_Scroll);
            // 
            // checkResize
            // 
            this.checkResize.AutoSize = true;
            this.checkResize.Checked = true;
            this.checkResize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkResize.Enabled = false;
            this.checkResize.Location = new System.Drawing.Point(132, 20);
            this.checkResize.Name = "checkResize";
            this.checkResize.Size = new System.Drawing.Size(106, 17);
            this.checkResize.TabIndex = 45;
            this.checkResize.Text = "Resize (960,540)";
            this.checkResize.UseVisualStyleBackColor = true;
            // 
            // numericOnBus
            // 
            this.numericOnBus.Location = new System.Drawing.Point(259, 145);
            this.numericOnBus.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericOnBus.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericOnBus.Name = "numericOnBus";
            this.numericOnBus.Size = new System.Drawing.Size(48, 20);
            this.numericOnBus.TabIndex = 56;
            this.numericOnBus.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericOnBus.ValueChanged += new System.EventHandler(this.numericOnBus_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(309, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "s/1 lần đếm";
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1359, 719);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.Cogfigure);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox2);
            this.Name = "formMain";
            this.Text = "Detect Vehicle";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.formMain_Load);
            this.Move += new System.EventHandler(this.formMain_Move);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDistance)).EndInit();
            this.Cogfigure.ResumeLayout(false);
            this.Cogfigure.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackConfidence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOnBus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonSetLine;
        private System.Windows.Forms.TextBox textBoxy2;
        private System.Windows.Forms.TextBox textBoxx2;
        private System.Windows.Forms.TextBox textBoxy1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRstp;
        private System.Windows.Forms.Button buttonMP4Play;
        private System.Windows.Forms.Button buttonRtspPlay;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.NumericUpDown numericFrame;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericDistance;
        private System.Windows.Forms.RadioButton radioYoloTiny;
        private System.Windows.Forms.RadioButton radioYolo;
        private System.Windows.Forms.GroupBox Cogfigure;
        private System.Windows.Forms.CheckBox checkResize;
        private System.Windows.Forms.TrackBar trackConfidence;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lbTotalMoto;
        private System.Windows.Forms.Label lbTotalCar;
        private System.Windows.Forms.Label lbTotalBus;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbDatetime;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkTracking;
        private System.Windows.Forms.NumericUpDown numericAngle;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkShowHideLine;
        private System.Windows.Forms.CheckBox checkShowHideDetect;
        private System.Windows.Forms.CheckBox checkAutostart;
        private System.Windows.Forms.TextBox textBoxx1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnStartTakePicture;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtsize;
        private System.Windows.Forms.Button btnCalSize;
        private System.Windows.Forms.CheckBox checkAutoSize;
        private System.Windows.Forms.CheckBox chkPerson;
        private System.Windows.Forms.CheckBox chkOnBus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericOnBus;
    }
}

