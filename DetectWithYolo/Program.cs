﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DetectWithYolo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new formMain());
            }
            catch (Exception) {
                string email = ConfigurationManager.AppSettings["email"];
                string billboardId = ConfigurationManager.AppSettings["billboardId"];
                string billboardCameraId = ConfigurationManager.AppSettings["billboardCameraId"];
                Gmail.SendMailTakePhoto(email, int.Parse(billboardId), int.Parse(billboardCameraId), true);
            }
        }
    }
}
