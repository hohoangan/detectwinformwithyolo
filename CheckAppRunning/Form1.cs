﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckAppRunning
{
    public partial class Form1 : Form
    {
        BackgroundWorker worker = new BackgroundWorker();
        public Form1()
        {
            InitializeComponent();
            worker.DoWork += Worker_DoWork;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            string path = ConfigurationManager.AppSettings["path"];
            txt2.Text = path;
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (worker.IsBusy == false)
                worker.RunWorkerAsync();
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (IsProcessOpen(txt1.Text.Trim()) == false)
                {
                    Process process = new Process();
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    startInfo.FileName = txt2.Text.Trim();
                    process.StartInfo = startInfo;
                    process.Start();

                    System.Threading.Thread.Sleep(60000*2);//1000 = 20s
                }
            }
            catch (Exception ex)
            {
            }
        }

        public bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }

            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(worker.IsBusy == false)
                worker.RunWorkerAsync();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        void BrowseFile()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            string currentPath = System.AppContext.BaseDirectory;
            fdlg.Title = "C# Corner Open File Dialog";
            fdlg.InitialDirectory = currentPath;
            fdlg.Filter = "Detect File (*.exe)|*.*|All files (*.*)|*.*";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                txt2.Text = fdlg.FileName;
                AppConfigChangeValue("path", fdlg.FileName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BrowseFile();
        }
        private void AppConfigChangeValue(string name, object value)
        {
            try
            {
                Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                configuration.AppSettings.Settings[name].Value = value.ToString();
                configuration.Save();
                ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception ex)
            {
                string funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                MessageBox.Show(string.Format("{0}: {1}", funcName, ex.Message));
            }
        }
    }
}
