﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordVideos
{
    public class Camera
    {
        public string Channel { get; set; }
        public int BillboardId { get; set; }
        public int CampaignId { get; set; }
        public int HTTPPort { get; set; }
        public string HTTPLink { get; set; }
        public string Domain { get; set; }
    }
    public class ListCamera
    {
        public List<Camera> Cameras { get; set; }
    }
}
