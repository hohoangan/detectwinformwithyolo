﻿
namespace RecordVideos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.bthBrowse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbSoluongBB = new System.Windows.Forms.Label();
            this.btnMakeMP4 = new System.Windows.Forms.Button();
            this.chkPROD = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(732, 639);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // bthBrowse
            // 
            this.bthBrowse.Location = new System.Drawing.Point(738, 11);
            this.bthBrowse.Name = "bthBrowse";
            this.bthBrowse.Size = new System.Drawing.Size(75, 23);
            this.bthBrowse.TabIndex = 1;
            this.bthBrowse.Text = "Browse";
            this.bthBrowse.UseVisualStyleBackColor = true;
            this.bthBrowse.Click += new System.EventHandler(this.bthBrowse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(739, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Số lượng BB: ";
            // 
            // lbSoluongBB
            // 
            this.lbSoluongBB.AutoSize = true;
            this.lbSoluongBB.Location = new System.Drawing.Point(818, 42);
            this.lbSoluongBB.Name = "lbSoluongBB";
            this.lbSoluongBB.Size = new System.Drawing.Size(13, 13);
            this.lbSoluongBB.TabIndex = 3;
            this.lbSoluongBB.Text = "0";
            // 
            // btnMakeMP4
            // 
            this.btnMakeMP4.Location = new System.Drawing.Point(821, 11);
            this.btnMakeMP4.Name = "btnMakeMP4";
            this.btnMakeMP4.Size = new System.Drawing.Size(75, 23);
            this.btnMakeMP4.TabIndex = 4;
            this.btnMakeMP4.Text = "RUN";
            this.btnMakeMP4.UseVisualStyleBackColor = true;
            this.btnMakeMP4.Click += new System.EventHandler(this.btnMakeMP4_Click);
            // 
            // chkPROD
            // 
            this.chkPROD.AutoSize = true;
            this.chkPROD.Checked = true;
            this.chkPROD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPROD.Location = new System.Drawing.Point(902, 15);
            this.chkPROD.Name = "chkPROD";
            this.chkPROD.Size = new System.Drawing.Size(57, 17);
            this.chkPROD.TabIndex = 5;
            this.chkPROD.Text = "PROD";
            this.chkPROD.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 639);
            this.Controls.Add(this.chkPROD);
            this.Controls.Add(this.btnMakeMP4);
            this.Controls.Add(this.lbSoluongBB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bthBrowse);
            this.Controls.Add(this.richTextBox1);
            this.Name = "Form1";
            this.Text = "Record Cameras";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button bthBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbSoluongBB;
        private System.Windows.Forms.Button btnMakeMP4;
        private System.Windows.Forms.CheckBox chkPROD;
    }
}

