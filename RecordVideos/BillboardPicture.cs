﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordVideos
{
    class BillboardPicture
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int CampaignId { get; set; }
        public DateTime? LastUpdated { get; set; } = DateTime.UtcNow.AddHours(7);
        public int BillboardId { get; set; }
        public int PictureTypeId { get; set; }
        public bool AcceptanceImage { get; set; } = false;
    }
    class DigitalBillboardPicture
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int CampaignId { get; set; }
        public DateTime? LastUpdated { get; set; } = DateTime.UtcNow.AddHours(7);
        public int DigitalBillboardId { get; set; }
        public int PictureTypeId { get; set; }
        public bool AcceptanceImage { get; set; } = false;
    }
}
