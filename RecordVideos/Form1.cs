﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace RecordVideos
{
    public partial class Form1 : Form
    {
        public const string SqlConnectionDEV = "Data Source=lk-db-dev.database.windows.net;Initial Catalog=LK_db_dev;User ID=anho;Password=An7602119211;MultipleActiveResultSets=True;";
        public const string SqlConnectionPRODUCT = "Data Source=lk-db-prod-server.database.windows.net;Initial Catalog=LK_db_prod;User ID=anho;Password=An7602119211;MultipleActiveResultSets=True;";
        public const string AzureBlobStorageConnectionStringDEV = "DefaultEndpointsProtocol=https;AccountName=lkstoragedev;AccountKey=oQgLf9o70WJnZjrSBXoY5bywOqIqUCaE+VU965kFhIGoZ02dXEIoIe29QPtdD+WI+rofRcfFXe0mO8JirwgnmA==;EndpointSuffix=core.windows.net";
        public const string AzureBlobStorageConnectionStringPRODUCT = "DefaultEndpointsProtocol=https;AccountName=lkstorageprod;AccountKey=HQGAhAiOMgELY/KKfhShx1twJ9eFJy/yQingNtRCPeVOnv5fT1zzVOcyMVVv1run5oeh0SGPHi5xXqYKvouDMQ==;EndpointSuffix=core.windows.net";
        public const string BlodContainerDEV = "lkmediadata-dev";
        public const string BlodContainerPRODUCT = "lkmediadata-prod";
        public const string DomainDEV = "lk-backend-dev";
        public const string DomainPRODUCT = "lk-backend-prod";
        string domain = string.Empty;

        BackgroundWorker _backgroundWorker = new BackgroundWorker();
        BackgroundWorker _load = new BackgroundWorker();
        bool loading = false;
        List<Camera> cameras;
        public Form1()
        {
            InitializeComponent();
            domain = DomainDEV;//DomainPRODUCT
            if(chkPROD.Checked == true)
                domain = DomainPRODUCT;

            // Set up the Background Worker Events 
            _backgroundWorker.DoWork += _backgroundWorker_DoWork;
            _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;
            _load.DoWork += _load_DoWork; ;
            _load.RunWorkerCompleted += _load_RunWorkerCompleted;
        }

        private void bthBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JSON file |*.json";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.Clear();
                lbSoluongBB.Text = "0";
                string filename = openFileDialog.FileName;
                UpdateForm(string.Format("File Name: {0}", filename), Color.Green, true);
                string content = System.IO.File.ReadAllText(filename);
                JavaScriptSerializer j = new JavaScriptSerializer();
                var listCameras = j.Deserialize(content, typeof(List<Camera>));
                cameras = listCameras as List<Camera>;
                lbSoluongBB.Text = cameras.Count().ToString();
            }
        }

        private void Execute(List<Camera> cameras)
        {
            UpdateForm(string.Format("-------------BEGIN : {0}-------------", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")), Color.Green, true);
            try
            {
                cameras.ForEach(async item =>
                {
                    UpdateForm(string.Format("{0} {1}: ", item.Channel, item.BillboardId), Color.Black, true);
                    await AutoTakePicture(item);
                    //make vid
                    string fileName = await makeAvi(item.BillboardId.ToString());
                    //update file
                    if (File.Exists(fileName))
                    {
                        await UploadFileMp4(fileName, item);
                    }
                    UpdateForm(string.Format("{0} {1}: {2}", item.Channel, item.BillboardId, "Done"), Color.Green, true);
                    UpdateForm(string.Format("-------------------------------"), Color.Black, true);
                });
            }
            catch (Exception ex)
            {
                loading = false;
                UpdateForm(ex.Message, Color.Red, true);
            }
            finally { }
        }

        #region QUAY VIDEO DIGITAL
        public async Task<string> GetImgLink(Camera camera)
        {
            try
            {
                HttpClient client = new HttpClient();
                String domain = camera.Domain;// "364nguyenkiem.ddns.net";
                //var url = "admin:vinstar@789@364nguyenkiem.ddns.net:81/ISAPI/Streaming/Channels/102/picture";
                var url = camera.HTTPLink;
                if (string.IsNullOrEmpty(url))
                    return "";
                var userInfo = url.Substring(0, url.IndexOf(domain, StringComparison.Ordinal));
                url = url.Replace(userInfo, "");
                var uri = new Uri("http://"+url);

                if (userInfo != "")
                {
                    userInfo = userInfo.Substring(0, userInfo.Length - 1);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes(userInfo)));
                }
                var stream = await client.GetStreamAsync(uri);
                string path = Path.Combine("IMG",camera.BillboardId.ToString());
                //string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";

                if(Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                System.IO.DirectoryInfo myDir = new DirectoryInfo(path);
                int count = myDir.GetFiles().Length;
                string fileName = "IMG_"+count.ToString().PadLeft(2,'0') + ".png";

                Image img = System.Drawing.Image.FromStream(stream);
                img.Save(Path.Combine(path, fileName), ImageFormat.Png);
                return path;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public async Task AutoTakePicture(Camera camera)
        {
            try
            {
                UpdateForm(string.Format("{0} {1}: {2}", camera.Channel, camera.BillboardId, "Get img -> begin"), Color.Black, true);
                for (int i = 0; i < 100; i++)
                {
                    Thread.Sleep(4000);//4s
                    string url = await GetImgLink(camera);
                }
                UpdateForm(string.Format("{0} {1}: {2}", camera.Channel, camera.BillboardId, "Get img -> done"), Color.Green, true);
            }
            catch (Exception ex)
            {
                loading = false;
                UpdateForm(ex.Message, Color.Red, true);
            }
        }

        private async Task<string> makeAvi(string folder)
        {
            string fileName = "IMG/"+ folder + ".mp4";
            // reads all images in folder 
            using (var process = new Process())
            {
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.FileName = @"ffmpeg.exe";
                //process.StartInfo.Arguments = @"-y -framerate 1/2 -start_number 01 -i img\IMG_%2d.png -vf scale=1280:720 -c:v libx264 -r 30 -pix_fmt yuv420p img\out.mp4";
                process.StartInfo.Arguments = @"-y -framerate 10 -start_number 00 -i IMG\"+ folder+ @"\IMG_%2d.png -vf scale=1280:720 -c:v libx264 -r 30 -pix_fmt yuv420p " + fileName;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.BeginErrorReadLine();
                process.WaitForExit();
                return fileName;
            }
        }

        private async Task<string> UploadFileMp4(string filePath, Camera camera = null)
        {
            try
            {
                string connectBlod = AzureBlobStorageConnectionStringDEV;
                string containerName = BlodContainerDEV;
                if (1 == 0)
                {
                    connectBlod = AzureBlobStorageConnectionStringPRODUCT;
                    containerName = BlodContainerPRODUCT;
                }
                BlobContainerClient containerClient = new BlobContainerClient(connectBlod, containerName);
                string fileName = string.Format(@"{0}.mp4", Guid.NewGuid());
                var blobClient = containerClient.GetBlobClient(fileName);

                using (FileStream stream = File.Open(filePath, FileMode.Open))
                {
                    UpdateForm(string.Format("{0} {1}: {2}", camera.Channel, camera.BillboardId, "Upload file -> begin"), Color.Black, true);
                    await blobClient.UploadAsync(stream, new BlobHttpHeaders { ContentType = "mp4" });
                    if (!string.IsNullOrEmpty(blobClient.Uri.AbsoluteUri))
                    {
                        UpdateForm(string.Format("{0} {1}: {2} {3}", camera.Channel, camera.BillboardId, "Upload file ->", blobClient.Uri.AbsoluteUri), Color.Green, true);
                        await Save2Db(blobClient.Uri.AbsoluteUri, camera);
                        UpdateForm(string.Format("{0} {1}: {2}", camera.Channel, camera.BillboardId, "Save DB -> done"), Color.Purple, true);
                        stream.Dispose();
                        File.Delete(filePath);
                        string folderPath = Path.Combine("IMG", camera.BillboardId.ToString());
                        System.IO.DirectoryInfo myDir = new DirectoryInfo(folderPath);
                        myDir.Delete(true);
                    }
                }
                return blobClient.Uri.AbsoluteUri;
            }
            catch (Exception ex)
            {
                loading = false;
                UpdateForm(ex.Message, Color.Red, true);
                return "";
            }
        }

        private async Task Save2Db(string url, Camera camera)
        {
            try
            {
                if (!string.IsNullOrEmpty(url))
                {
                    DateTime dtVn = DateTime.UtcNow.AddHours(7);
                    //var bbpic = new BillboardPicture
                    //{
                    //    BillboardId = camera.BillboardId,
                    //    CampaignId = camera.CampaignId,
                    //    LastUpdated = DateTime.UtcNow,
                    //    Id = 0,
                    //    AcceptanceImage = false,
                    //    PictureTypeId = dtVn.Hour < 18 && dtVn.Hour > 5 ? 1 : 2, //1 Day: 2 Night
                    //    Url = url
                    //};
                    //List<BillboardPicture> pics = new List<BillboardPicture>();
                    //pics.Add(bbpic);
                    List<object> picsObj = new List<object>();
                    if (camera.Channel == "BB") {
                        var pic = new BillboardPicture
                        {
                            BillboardId = camera.BillboardId,
                            CampaignId = camera.CampaignId,
                            LastUpdated = DateTime.UtcNow,
                            Id = 0,
                            AcceptanceImage = false,
                            PictureTypeId = dtVn.Hour < 18 && dtVn.Hour > 5 ? 1 : 2, //1 Day: 2 Night
                            Url = url
                        };
                        picsObj.Add(pic);
                    }
                    else if (camera.Channel == "Led")
                    {
                        var pic = new DigitalBillboardPicture
                        {
                            DigitalBillboardId = camera.BillboardId,
                            CampaignId = camera.CampaignId,
                            LastUpdated = DateTime.UtcNow,
                            Id = 0,
                            AcceptanceImage = false,
                            PictureTypeId = dtVn.Hour < 18 && dtVn.Hour > 5 ? 1 : 2, //1 Day: 2 Night
                            Url = url
                        };
                        picsObj.Add(pic);
                    }

                    if (picsObj.Count > 0)
                    {
                        //channel = "BB"
                        string link = string.Format("https://{0}.azurewebsites.net/api/billboard/pictures", domain);
                        if (camera.Channel == "Led")
                            link = string.Format("https://{0}.azurewebsites.net/api/digitalbillboard/pictures", domain);

                        var client = new RestClient(link);
                        client.Timeout = -1;
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Content-Type", "application/json");
                        string body = JsonSerializer.Serialize(picsObj);
                        request.AddParameter("application/json", body, ParameterType.RequestBody);
                        IRestResponse response = client.Execute(request);
                    }
                }
            }
            catch(Exception ex){
                loading = false;
                UpdateForm(ex.Message, Color.Red, true);
            }
        }
        #endregion

        private void btnMakeMP4_Click(object sender, EventArgs e)
        {
            if (!_backgroundWorker.IsBusy)
                _backgroundWorker.RunWorkerAsync();
        }

        #region Rich text box
        private void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!_backgroundWorker.IsBusy)
                _backgroundWorker.RunWorkerAsync();
        }

        private void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<int> hour = new List<int> { 8, 11, 14, 18 };
            DateTime dtVn = DateTime.UtcNow.AddHours(7);
            if (!hour.Contains(dtVn.Hour))
                return;
            else
            {
                Execute(cameras);
                //Thread.Sleep((10 * 60 * 1000) + 1);// ~ 0:10:01
                Thread.Sleep((60 * 60 * 1000) + 1);// ~ 1:00:01
            }
        }

        void UpdateForm(string content, Color color, bool newrow = false, double percent = 0)
        {
            try
            {
                MethodInvoker methodInvokerDelegate = delegate ()
                {
                    if (newrow == true)
                        richTextBox1.AppendText("\r\n");

                    richTextBox1.SelectionStart = richTextBox1.TextLength;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.SelectionColor = color;
                    richTextBox1.AppendText(content);
                    richTextBox1.SelectionColor = richTextBox1.ForeColor;

                    richTextBox1.ScrollToCaret();

                    string createText = richTextBox1.Text;
                    File.WriteAllText("IMG/Log.txt", createText);
                };

                //This will be true if Current thread is not UI thread.
                if (this.InvokeRequired)
                    this.Invoke(methodInvokerDelegate);
                else
                    methodInvokerDelegate();
            }
            catch (Exception)
            {
            }
        }

        private void _load_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _load.RunWorkerAsync();
        }

        private void _load_DoWork(object sender, DoWorkEventArgs e)
        {
            if (loading == false)
                return;
            MethodInvoker methodInvokerDelegate = delegate ()
            {
                string[] str = { "-", "\\", "|", "/", "-" };
                for (int i = 0; i < str.Length; i++)
                {
                    //add text
                    richTextBox1.Select(richTextBox1.TextLength - 1, 0);
                    richTextBox1.AppendText(str[i]);
                    //remove cái cũ
                    richTextBox1.SelectionStart = richTextBox1.TextLength - 1;
                    richTextBox1.SelectionLength = 1;
                    richTextBox1.SelectionColor = Color.Red;
                    richTextBox1.SelectedText = "";
                    Thread.Sleep(200);
                }
            };

            //This will be true if Current thread is not UI thread.
            if (this.InvokeRequired)
                this.Invoke(methodInvokerDelegate);
            else
                methodInvokerDelegate();
        }
        #endregion
    }
}
